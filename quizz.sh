#!/bin/bash

nb=$(ls -l ~/Videos/ | grep -E "*.mp4" | wc -l)
let "titreauhasard=$RANDOM%$nb"
let "titreauhasard=$titreauhasard+1"

titreauhasard=$(ls ~/Videos/ | grep -E "*.mp4" | head -n $titreauhasard | tail -n 1)

vlc ~/Videos/$titreauhasard --start-time 1000 & 

PID=$(ps | grep vlc | head -2 | tail -1 | tr -s " "| cut -d " " -f 2)
sleep 20
kill -9 $PID

echo "Quel est le titre de ce film : "
echo "(minuscules et separateur : _ )"

read titredufilm

titredufilm="$titredufilm".mp4

if [ "$titredufilm" == "$titreauhasard" ]

then
	echo "Vous avez trouvé le bon film"
	echo "BRAVO !!"
	sleep 1

else
	echo "Euuuhhhh non c'est pas ce film !!!"
	echo "Retentez votre chance..."
	sleep 1
fi
