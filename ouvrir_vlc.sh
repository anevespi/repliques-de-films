#!/bin/bash

#cette fonction reçoit en param 1 le nom du fichier et en param 2 le n° de ligne

extraction_tc ()
{
	timer=$(cat $1 | head -n $2 | tail -n 1)
	chaineD=${timer:0:8}
	echo $chaineD
}



conversion()
{
	T_srt=$1
	
	h=$(echo $T_srt | cut -d ':' -f1)

	let "H=$h*3600"

	min=$(echo $T_srt | cut -d ':' -f2)

	let "M=$min*60"

	S=$(echo $T_srt | cut -d ':' -f3)

	let "T_vlc=$H+$M+$S"
	
	echo $T_vlc
}

timecode=$(extraction_tc $1 $2)
timecode=$(conversion $timecode)
echo 'timecode' $timecode

echo "vlc $1 --start-time $timecode"
longueur=$(expr length $1)

check=$(echo $1 | grep ".fr.srt")


if [ -z $check ]
then
        let "longueur=$longueur - 4"
        nomFilm=${1:0:longueur}.mp4
else
        let "longueur=$longueur - 7"
        nomFilm=${1:0:longueur}.mp4
fi

vlc $nomFilm --start-time $timecode
