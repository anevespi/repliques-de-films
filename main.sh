#!/bin/bash

choix=1

echo "--------------APPLICATION REPLIQUES DE FILMS-------------"
sleep 1

while [ $choix -gt 0 ] && [ $choix -lt 4 ] 
# quittera si 4 ou sup
do

echo "----------------------------------------------------------"
sleep 1
echo "PROGRAMME PRINCIPAL"
echo "1 Donnez la replique"
echo "2 Quizz"
echo "3 Espeak"
echo "4 Quitter"

read choix

case $choix in 
1) 
echo "Tapez la réplique" 
read replique
echo "Vous avez tapé $replique"

#implémentation fonction recherche pour avoir la réplique
source recherche
resultat=$(recherche_timecode_partout "$replique")
ssT=$(echo $resultat | cut -d ':' -f1)
num=$(echo $resultat | cut -d ':' -f2)


./ouvrir_vlc.sh $ssT $num

#mettre fonction recherche avec choix des films
#et confirmation utilisateur
;;


2)
./quizz.sh
;;

3)
echo "Tapez la réplique" 
read replique

source recherche
chaine=$(recherche_timecode_partout "$replique")

ssT=$(echo $chaine | cut -d ':' -f1 )
num=$(echo $chaine |  cut -d ':' -f2)
./espeak $ssT $num
;;

*)
echo "FIN DU PROGRAMME"
;;

esac

done
